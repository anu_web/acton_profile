<?php
/**
 * @file
 * Administration implementations.
 */

/**
 * Acton profile admin UI.
 */
class ActonProfileUIController extends EntityDefaultUIController {
  /**
   * Tweak default menu items.
   */
  public function hook_menu() {
    $items = parent::hook_menu();

    $file_path = drupal_get_path('module', 'acton_profile');

    $items[$this->path]['title'] = 'Manage Acton profiles';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    $items[$this->path . '/list'] = array(
      'title' => 'Profiles',
      'description' => 'Show an overview of all Acton profiles.',
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );

    $items[$this->path . '/add']['title'] = 'Add new profile';
    $items[$this->path . '/add']['title callback'] = 't';
    $items[$this->path . '/add']['weight'] = -10;
    unset($items[$this->path . '/add']['title callback']);
    unset($items[$this->path . '/add']['title arguments']);

    if (variable_get('acton_profile_default_fetch_format', 'json') == 'json') {
      $items[$this->path . '/add-site-id'] = array(
        'title' => 'Add from site ID',
        'description' => 'Add a new profile by attempting to pre-fill using data from the default style server.',
        'page callback' => 'acton_profile_admin_add_site_id',
        'access arguments' => array('administer acton profiles'),
        'type' => MENU_LOCAL_ACTION,
        'file' => 'acton_profile.admin.inc',
        'file path' => $file_path,
      );
    }

    $items[$this->path . '/import']['title'] = 'Import profile';
    $items[$this->path . '/import']['weight'] = 10;
    unset($items[$this->path . '/import']['title callback']);
    unset($items[$this->path . '/import']['title arguments']);

    $items[$this->path . '/set-default/%acton_profile'] = array(
      'page callback' => 'acton_profile_admin_set_default',
      'page arguments' => array(4),
      'access arguments' => array('administer acton profiles'),
      'type' => MENU_CALLBACK,
      'file' => 'acton_profile.admin.inc',
      'file path' => $file_path,
    );

    return $items;
  }

  /**
   * Modify overview form.
   */
  public function overviewForm($form, &$form_state) {
    $form['#attached']['css'] = array(
      drupal_get_path('module', 'acton_profile') . '/acton-profile.css',
    );

    if (!variable_get('acton_profile_default_profile') && $profiles = acton_profile_load_multiple()) {
      // Set first profile as default if none set.
      reset($profiles);
      $key = key($profiles);

      // Redirect with token to force overlay parent refresh.
      drupal_goto($this->path . '/set-default/' . $key, array('query' => array('token' => '')));
    }

    return parent::overviewForm($form, $form_state);
  }

  /**
   * Modify overview table.
   */
  public function overviewTable($conditions = array()) {
    $table = parent::overviewTable($conditions);
    $table['#attributes']['class'][] = 'acton-profile-overview-table';
    return $table;
  }

  /**
   * Modify overview table row.
   */
  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $row = parent::overviewTableRow($conditions, $id, $entity, $additional_cols);

    $default = variable_get('acton_profile_default_profile');
    if ($entity->machine_name == $default) {
      // Show as default profile.
      $row[0]['data']['#prefix'] = '<span class="default">';
      $row[0]['data']['#suffix'] = ' <span class="label-default">(default profile)</span></span>';
    }
    else {
      // Show link to set it as active.
      $row[0]['data']['#suffix'] = '<span class="link-set-default">' . l(t('Set default'), 'admin/appearance/acton-profile/set-default/' . $entity->machine_name) . '</span>';
    }

    return $row;
  }
}

/**
 * Generates the Acton profile editing form.
 */
function acton_profile_form($form, &$form_state, ActonProfile $profile, $op = 'edit') {
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'acton_profile') . '/acton-profile.css',
  );

  static $titles = array(
    'edit' => 'Edit profile !profile',
    'add' => 'Add new profile',
    'clone' => 'Clone profile !profile',
  );
  drupal_set_title(t($titles[$op], array('!profile' => strip_tags($profile ? $profile->name : ''))));

  if ($op == 'clone') {
    // Reset for cloning.
    $profile->name .= ' (cloned)';
    $profile->machine_name = '';
  }

  // Add default profile settings.
  $profile->settings += array(
    'search_show' => FALSE,
    'search_base' => array(),
    'search_advanced' => TRUE,
    'server_override' => FALSE,
    'server_endpoint_base' => '',
    'server_endpoint_suffix' => '',
    'server_fetch_format' => '',
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile name'),
    '#default_value' => $profile->name,
    '#required' => TRUE,
  );
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $profile->machine_name,
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => 'acton_profile_load',
    ),
    '#disabled' => $op == 'edit',
  );

  // Set up data vertical tab set.
  $form['data'] = array(
    '#type' => 'vertical_tabs',
    '#title' => t('Profile data'),
    '#tree' => TRUE,
    '#default_tab' => 'basic',
  );
  acton_profile_get_profile_data_form($form, $form_state, $profile, $op);

  // Add settings fieldset.
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => !empty($profile->settings['search_show']) || !empty($profile->settings['server_override']),
  );
  // TODO Remove once responsive styles are the only version.
  $form['settings']['style_version'] = array(
    '#type' => 'select',
    '#title' => t('Style version'),
    '#default_value' => $profile->getStyleVersion(),
    '#options' => array(
      3 => t('Version 3'),
      4 => t('Version 4 (responsive design)'),
    ),
  );
  $form['settings']['acton_gateway_navigation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display horizontal navigation'),
    '#default_value' => !empty($profile->settings['acton_gateway_navigation']),
    '#description' => t('Display the top-level !main_links as the horizontal navigation bar. This option only applies if the profile style is set to responsive design, and then only if horizontal navigation is approved by Webstyle.', array(
      '!main_links' => l(t('Main links'), 'admin/structure/menu/settings'),
    )),
    '#states' => array(
      'visible' => array(
        'select[name="settings[style_version]"]' => array('value' => 4),
      ),
    ),
  );

  // Add basic settings.
  $form['settings']['metadata_use'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use profile for page metadata.'),
    '#description' => t('Acton Profile can provide the following metadata: identifier, title, description, subject, keywords, date modified, date for review, creator, creator email.'),
    '#default_value' => !empty($profile->settings['metadata_use']),
    '#parents' => array('settings', 'metadata_use'),
  );

  // Add search settings.
  $form['settings']['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search box'),
  );
  $form['settings']['search']['search_show'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show search box'),
    '#default_value' => !empty($profile->settings['search_show']),
    '#description' => t('Display the ANU search box to enable searching with ANU search engine by Funnelback. This is different from the Drupal search box.'),
    '#parents' => array('settings', 'search_show'),
  );
  $form['settings']['search']['search_base'] = array(
    '#type' => 'textarea',
    '#title' => t('Search base URLs'),
    '#default_value' => !empty($profile->settings['search_base']) ? implode("\n", $profile->settings['search_base']) : '',
    '#description' => t('The URLs under which to return Funnelback search results, one per line. Do not include the protocol (e.g. http://). Only works if ANU search is enabled, above. If nothing is entered, this defaults to the Site URL (above). If you have a different URL, or more than one, for search results you MUST set this value to the desired domain(s).'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[search_show]"]' => array('checked' => TRUE),
      ),
    ),
    '#parents' => array('settings', 'search_base'),
  );
  $form['settings']['search']['search_advanced'] = array(
    '#type' => 'checkbox',
    '#title' => t('Advanced search link'),
    '#default_value' => !empty($profile->settings['search_advanced']),
    '#description' => t('Check this option to display a link to the advanced search form.'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[search_show]"]' => array('checked' => TRUE),
      ),
    ),
    '#parents' => array('settings', 'search_advanced'),
  );

  // Add style server settings.
  $form['settings']['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Style server'),
  );
  $form['settings']['server']['server_override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override default style server'),
    '#default_value' => !empty($profile->settings['server_override']),
    '#parents' => array('settings', 'server_override'),
  );
  $server_override_states = array(
    'visible' => array(
      ':input[name="settings[server_override]"]' => array('checked' => TRUE),
    ),
  );
  global $is_https;
  $form['settings']['server']['server_endpoint_base'] = array(
    '#type' => 'textfield',
    '#title' => t('Style server base'),
    '#default_value' => $profile->settings['server_endpoint_base'],
    '#description' => t('Use this style server base instead of the default base. The default base is: !base', array('!base' => '<strong>' . variable_get('acton_profile_default_endpoint_base', ACTON_PROFILE_DEFAULT_ENDPOINT_BASE) . '</strong>')),
    '#field_prefix' => $is_https ? 'https://' : 'http://',
    '#states' => $server_override_states,
    '#parents' => array('settings', 'server_endpoint_base'),
  );
  $form['settings']['server']['server_endpoint_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Style server suffix'),
    '#default_value' => $profile->settings['server_endpoint_suffix'],
    '#description' => t('Use this style server suffix instead of the default suffix. The default suffix is: !suffix', array('!suffix' => '<strong>' . variable_get('acton_profile_default_endpoint_suffix', ACTON_PROFILE_DEFAULT_ENDPOINT_SUFFIX) . '</strong>')),
    '#states' => $server_override_states,
    '#parents' => array('settings', 'server_endpoint_suffix'),
  );
  $formats = array(NULL => t('(No format override)')) + acton_profile_get_fetch_formats();
  $form['settings']['server']['server_fetch_format'] = array(
    '#type' => 'radios',
    '#title' => t('Fetch format'),
    '#options' => $formats,
    '#default_value' => $profile->settings['server_fetch_format'],
    '#description' => filter_xss(t('Use this fetch format instead of the default format. The default format is: !format', array('!format' => '<strong>' . $formats[variable_get('acton_profile_default_fetch_format', 'json')] . '</strong>'))),
    '#states' => $server_override_states,
    '#parents' => array('settings', 'server_fetch_format'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save profile'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Build the profile data entry form.
 *
 * We are custom building the form until http://drupal.org/node/1192120 gets
 * resolved and Entity API gets another release.
 */
function acton_profile_get_profile_data_form(&$form, &$form_state, ActonProfile $profile, $op = 'edit') {
  // Prepare form elements.
  $data = acton_profile_get_profile_data();
  $elements = array();
  foreach ($data as $key => $property) {
    $type = (!empty($property['type']) && $property['type'] == 'date') ? 'date' : 'textfield';
    $elements[$key] = array(
      '#type' => $type,
      '#title' => filter_xss($property['label']),
      '#default_value' => isset($profile->data[$key]) ? $profile->data[$key] : '',
      '#description' => filter_xss($property['description']),
      '#required' => !empty($property['required']),
      '#parents' => array('data', $key),
    );
  }

  // Adapt site URL element based on default values from current request state.
  global $is_https;
  $elements['site_url']['#field_prefix'] = $is_https ? 'https://' : 'http://';
  if ($op == 'add' && empty($profile->data['site_url'])) {
    $elements['site_url']['#default_value'] = $_SERVER['HTTP_HOST'];
  }

  // Set up field sets.
  $fieldsets = array(
    'info' => t('Basic information'),
    'contacts' => t('Contacts'),
    'metadata' => t('Metadata'),
    'misc' => t('Miscellaneous'),
  );
  $fields = array(
    'info' => array(
      'site_short_name',
      'site_id',
      'site_url',
    ),
    'contacts' => array(
      'resp_officer',
      'resp_officer_address',
      'site_contact',
      'site_contact_address',
    ),
    'metadata' => array(
      'date_modified',
      'date_review',
      'meta_description',
      'meta_subject',
    ),
    'misc' => array(
      'about_page',
    ),
  );

  // Construct the form.
  foreach ($fieldsets as $fieldset_key => $fieldset_title) {
    $form['data'][$fieldset_key] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($fieldset_title),
    );
    foreach ($fields[$fieldset_key] as $field_key) {
      $form['data'][$fieldset_key][$field_key] = $elements[$field_key];
    }
  }
}

/**
 * Acton profile editing form validate callback.
 */
function acton_profile_form_validate($form, &$form_state) {
  $data = $form_state['values']['data'];
  $settings = $form_state['values']['settings'];
  $profile = $form_state['acton_profile'];

  // Validate the site ID.
  if (variable_get('acton_profile_validate_site_id', TRUE) && !acton_profile_valid_site_id($data['site_id'], $profile->getServerEndpoint($settings))) {
    form_set_error('data][site_id', t('The site ID is invalid.'));
  }

  // Validate the site URL.
  if (!acton_profile_valid_base_url($data['site_url'])) {
    form_set_error('data][site_url', t('The site URL is invalid.'));
  }

  // Validate search base URLs.
  $search_base = array_filter(preg_split('/[\r\n]+/', trim($settings['search_base'])));
  foreach ($search_base as $base_url) {
    if (!acton_profile_valid_base_url($base_url)) {
      form_set_error('settings][search_base', t('One or more of the specified search base URLs are invalid.'));
      break;
    }
  }

  // Validate the server endpoint.
  if ($settings['server_endpoint_base'] || $settings['server_endpoint_suffix']) {
    $server_endpoint_base = $profile->getServerEndpointBase($settings);
    $server_endpoint_suffix = $profile->getServerEndpointSuffix($settings);
    if (!acton_profile_valid_endpoint($server_endpoint_base . $server_endpoint_suffix)) {
      // Only set error once, if at all.
      $server_errors = array_keys(array_filter(array(
        'settings][server_endpoint_base' => $settings['server_endpoint_base'],
        'settings][server_endpoint_suffix' => $settings['server_endpoint_suffix'],
      )));
      if ($server_error = array_pop($server_errors)) {
        form_set_error($server_error, t('The style server is invalid.'));
      }
      array_walk($server_errors, 'form_set_error');
    }
  }
}

/**
 * Acton profile editing form submit callback.
 */
function acton_profile_form_submit($form, &$form_state) {
  // Clean data.
  $search_base = &$form_state['values']['settings']['search_base'];
  $search_base = array_filter(preg_split('/[\r\n]+/', $search_base));
  unset($form_state['values']['data']['data__active_tab']);

  // Trim data values.
  foreach ($form_state['values']['data'] as $key => $value) {
    if (is_string($value)) {
      $form_state['values']['data'][$key] = trim($form_state['values']['data'][$key]);
    }
  }

  // Save profile.
  $profile = entity_ui_form_submit_build_entity($form, $form_state);
  $profile->save();

  drupal_set_message(t('The profile has been saved.'));
  $form_state['redirect'] = 'admin/appearance/acton-profile';

  acton_profile_clear_cache();
}

/**
 * Get the form to fetch a profile from the style server and pre-fill the
 * profile edit form.
 */
function acton_profile_admin_add_site_id() {
  $json = NULL;

  // Prepare step 1: Form for site ID input.
  if (!isset($_POST['form_id']) || $_POST['form_id'] == 'acton_profile_admin_add_site_id_form') {
    $form_state = array();

    // Get site ID entry form.
    $form = drupal_build_form('acton_profile_admin_add_site_id_form', $form_state);
    if (!empty($form_state['values']['json'])) {
      $json = $form_state['values']['json'];
    }
    else {
      // Exit when no result is available.
      return $form;
    }
  }

  // Prepare step 2: Entity form.
  $profile = NULL;
  if (is_array($json)) {
    // Prefill entity.
    $sitevars = $json['sitevars'];
    $profile = entity_create('acton_profile', array());
    $profile->name = decode_entities($sitevars['sitename']);
    $profile->data['site_short_name'] = $profile->name;
    $profile->data['site_id'] = $sitevars['site_id'];
    $profile->data['site_url'] = trim($sitevars['site_url'], '/');
  }
  $form = entity_ui_get_form('acton_profile', $profile, 'add');

  return $form;
}

/**
 * Fetch a profile from the style server.
 */
function acton_profile_admin_add_site_id_form($form, &$form_state) {
  $form['site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => isset($form_state['values']['site_id']) ? $form_state['values']['site_id'] : '',
    '#description' => t('Search for profile with this site ID. For a list of possible site IDs, please visit the !styleguide website.', array(
      '!styleguide' => l(t('Web Style Guide'), 'http://styles.anu.edu.au/guide/id.php', array('attributes' => array('target' => '_blank'))),
    )),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add profile'),
  );

  // Add placeholder for the fetched JSON object.
  $form['json'] = array(
    '#type' => 'value',
  );

  return $form;
}

/**
 * Profile fetch form validation handler.
 */
function acton_profile_admin_add_site_id_form_validate($form, &$form_state) {
  // Validate site ID.
  if (!acton_profile_valid_site_id($form_state['values']['site_id'])) {
    form_set_error('site_id', t('The ID cannot be found on the style server.'));
  }
}

/**
 * Profile fetch form submit handler.
 */
function acton_profile_admin_add_site_id_form_submit($form, &$form_state) {
  $endpoint_base = variable_get('acton_profile_default_endpoint_base', ACTON_PROFILE_DEFAULT_ENDPOINT_BASE);
  $endpoint_suffix = variable_get('acton_profile_default_endpoint_suffix', ACTON_PROFILE_DEFAULT_ENDPOINT_SUFFIX);
  $endpoint = $endpoint_base . $endpoint_suffix;
  if ($json = acton_profile_remote_fetch_json($endpoint, $form_state['values']['site_id'])) {
    $form_state['values']['json'] = $json;
  }
  $form_state['redirect'] = FALSE;
}

/**
 * Set a profile as default.
 */
function acton_profile_admin_set_default(ActonProfile $profile) {
  variable_set('acton_profile_default_profile', $profile->machine_name);
  acton_profile_clear_cache();

  drupal_set_message(t('The profile %name has been set as default.', array('%name' => $profile->name)));
  drupal_goto('admin/appearance/acton-profile');
}

/**
 * Module settings form.
 */
function acton_profile_settings() {
  $form = array();

  global $is_https;
  $form['acton_profile_default_endpoint_base'] = array(
    '#type' => 'textfield',
    '#title' => t('Default style server URL base'),
    '#description' => t('This style server URL base will be used by default. In addition, it will be used to perform JSON pre-fill requests. This option can be overridden in a profile.'),
    '#field_prefix' => $is_https ? 'https://' : 'http://',
    '#default_value' => variable_get('acton_profile_default_endpoint_base', ACTON_PROFILE_DEFAULT_ENDPOINT_BASE),
  );
  $form['acton_profile_default_endpoint_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Default style server URL suffix'),
    '#description' => t('This suffix is used by default after the URL base above to fetch style page parts. In addition, it will be used to perform JSON pre-fill requests. This option can be overridden in a profile.'),
    '#default_value' => variable_get('acton_profile_default_endpoint_suffix', ACTON_PROFILE_DEFAULT_ENDPOINT_SUFFIX),
  );

  $form['acton_profile_default_fetch_format'] = array(
    '#type' => 'radios',
    '#title' => t('Default intermediate fetch format'),
    '#options' => acton_profile_get_fetch_formats(),
    '#description' => t('When fetching style parts, using JSON allows partial profile pre-filling (this option determines whether the action "Add from site ID" is available). JSON transfer takes one request, while HTML parts take one transfer per part. This option can be overridden in a profile.'),
    '#default_value' => variable_get('acton_profile_default_fetch_format', 'json'),
  );

  $fetch_method_options = array();
  foreach (acton_profile_remote_get_fetch_methods() as $name => $method) {
    $fetch_method_options[$name] = $method['label'];
  }
  $form['acton_profile_fetch_method'] = array(
    '#type' => 'radios',
    '#title' => t('Fetch method'),
    '#options' => $fetch_method_options,
    '#default_value' => variable_get('acton_profile_fetch_method', 'direct_get'),
    '#description' => t('The method to use when fetching a style part.'),
  );

  $form['cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cached GET settings'),
    '#description' => t('The following options apply when the fetch method is Cached GET.'),
    '#states' => array(
      'visible' => array(
        ':input[name="acton_profile_fetch_method"]' => array('value' => 'cache_get'),
      ),
    ),
  );
  $form['cache']['acton_profile_cache_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache lifetime'),
    '#default_value' => variable_get('acton_profile_cache_lifetime', ACTON_PROFILE_DEFAULT_CACHE_LIFE),
    '#description' => t('How long (in seconds) before this cache expires.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['cache']['flush_now'] = array(
    '#type' => 'submit',
    '#value' => t('Flush cache now'),
    '#submit' => array('acton_profile_admin_cache_flush'),
  );

  $form['acton_profile_validate_site_id'] = array(
    '#type' => 'checkbox',
    '#title' => t('Validate site ID when saving profile'),
    '#description' => t('When saving a profile, this option specifies whether or not a provided site ID should be checked against the style server.'),
    '#default_value' => variable_get('acton_profile_validate_site_id', TRUE),
  );

  $form['#validate'][] = 'acton_profile_settings_validate';
  $form['#submit'][] = 'acton_profile_settings_submit';

  return system_settings_form($form);
}

/**
 * Module settings form validation handler.
 */
function acton_profile_settings_validate($form, &$form_state) {
  $endpoint_base = $form_state['values']['acton_profile_default_endpoint_base'];
  $endpoint_suffix = $form_state['values']['acton_profile_default_endpoint_suffix'];
  if (!acton_profile_valid_endpoint($endpoint_base . $endpoint_suffix)) {
    form_set_error('acton_profile_default_endpoint_base', t('The specified style server is invalid.'));
    form_set_error('acton_profile_default_endpoint_suffix');
  }
}

/**
 * Module settings form submit handler.
 */
function acton_profile_settings_submit($form, &$form_state) {
  // Rebuild menu if fetch format has changed.
  if ($form_state['values']['acton_profile_default_fetch_format'] != variable_get('acton_profile_default_fetch_format', 'json')) {
    variable_set('menu_rebuild_needed', TRUE);
  }

  acton_profile_clear_cache();
}

/**
 * Flush cache.
 */
function acton_profile_admin_cache_flush($form, &$form_state) {
  cache_clear_all('*', 'cache_acton_profile', TRUE);
  drupal_set_message(t('Web style cache has successfully been flushed.'));
}
