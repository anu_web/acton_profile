<?php
/**
 * @file
 * Functions for dealing with the remote style server.
 */

/**
 * Get the HTML content for a style part.
 *
 * @param string $part
 *   The name of the part to get as defined in the style guide.
 * @param ActonProfile $profile
 *   Profile to part for.
 * @param array $params
 *   Extra HTTP parameters.
 *
 * @return
 *   HTML content.
 */
function acton_profile_remote_get_part($part, ActonProfile $profile, array $params = array()) {
  if ($part == 'json') {
    // Abort if trying to fetch 'json'.
    return;
  }
  if (!isset($profile)) {
    // Abort if no profile to get part for.
    return;
  }

  // Determine fetch format.
  $fetch_format = $profile->settings['server_fetch_format'];
  $formats = acton_profile_get_fetch_formats();
  if (!isset($formats[$fetch_format])) {
    // Use default method.
    $fetch_format = variable_get('acton_profile_default_fetch_format', 'json');
  }

  // Initialise lazy fetching.
  $parts = &drupal_static(__FUNCTION__);
  if (!isset($parts)) {
    $parts = array();
  }

  // Perform lazy fetch.
  $id = $profile->machine_name;
  $params_id = _acton_profile_params_hash($params);
  $cid = "acton_profile:parts:$id:$part:$params_id";
  $static = &$parts[$cid];
  if (!isset($static)) {
    $cache = cache_get($cid);
    if (isset($cache->data)) {
      // Use Drupal cache.
      $static = $cache->data;
    }
    else {
      // Fetch part.
      $fetch_part = acton_profile_remote_fetch_part($part, $profile, $fetch_format, $params);
      if ($fetch_part) {
        // Only validate fetched part if not empty.
        $static = $fetch_part;
        cache_set($cid, $static);
      }
    }
  }

  return $static;
}

/**
 * Get formats for fetching page parts.
 */
function acton_profile_get_fetch_formats() {
  return array(
    'html' => t('HTML parts'),
    'json' => t('JSON'),
  );
}

/**
 * Fetch a page style part for a profile.
 *
 * @param string $part
 *   Name of the page part to fetch.
 * @param ActonProfile $profile
 *   Profile to fetch.
 * @param $format
 *   Format to fetch and process. Can be 'json' or 'html'.
 * @param array $params
 *   Extra HTTP parameters.
 *
 * @return
 *   Page part HTML content.
 */
function acton_profile_remote_fetch_part($part, ActonProfile $profile, $format = 'json', array $params = array()) {
  $endpoint = $profile->getServerEndpoint();
  $site_id = $profile->data['site_id'];
  // TODO Remove once responsive styles are the only version.
  $ver = $profile->getStyleVersion();
  $params += array('ver' => $ver);

  // Fetch part depending on given format.
  if ($format == 'json') {
    $json = acton_profile_remote_fetch_json($endpoint, $site_id, $params);
    return is_string($json[$part]) ? $json[$part] : NULL;
  }
  elseif ($format == 'html') {
    return acton_profile_remote_fetch_site_part($endpoint, $site_id, $part, $params);
  }
}

/**
 * Fetch a page part through a specified method.
 *
 * @param string $endpoint
 *   Style server endpoint (not including the protocol).
 * @param $site_id
 *   ANU site ID.
 * @param string $part
 *   Name of the part to fetch.
 * @param array $params
 *   Extra HTTP parameters.
 *
 * @return
 *   Part content.
 */
function acton_profile_remote_fetch_site_part($endpoint, $site_id, $part, array $params = array()) {
  // Determine fetch method.
  $method = &drupal_static(__FUNCTION__ . ':method');
  if (!isset($method)) {
    $methods = acton_profile_remote_get_fetch_methods();
    $method = variable_get('acton_profile_fetch_method', 'direct_get');
    if (!isset($methods[$method])) {
      // Use direct GET by default. Here we rely on acton_profile_remote_get_fetch_methods() to cache efficiently.
      $method = 'direct_get';
    }
  }

  // Fetch.
  $parts = &drupal_static(__FUNCTION__ . ':parts', array());
  $params_id = _acton_profile_params_hash($params);
  $cid = "$endpoint:$site_id:$part:$params_id";
  $static = &$parts[$cid];
  if (!$static) {
    $static = acton_profile_remote_fetch_part_method($endpoint, $site_id, $part, $method, $params);
  }

  return $static;
}

/**
 * Fetch a JSON array for a site ID.
 *
 * @param string $endpoint
 *   Style server endpoint (not including the protocol).
 * @param $site_id
 *   ANU site ID.
 * @param array $params
 *   Extra HTTP parameters.
 *
 * @return
 *   Site JSON array.
 */
function acton_profile_remote_fetch_json($endpoint, $site_id, array $params = array()) {
  if ($json = acton_profile_remote_fetch_site_part($endpoint, $site_id, 'json', $params)) {
    $json_array = drupal_json_decode($json);
    if (is_array($json_array) && !empty($json_array)) {
      return $json_array;
    }
  }
}

/**
 * Fetch a part through a specified method.
 *
 * @param string $endpoint
 *   Style server endpoint (not including the protocol).
 * @param $site_id
 *   ANU site ID.
 * @param string $part
 *   Name of the part to fetch.
 * @param $method
 *   Fetch method. Must match a method declared in hook_acton_profile_fetch_methods().
 *
 * @return
 *   Part content.
 */
function acton_profile_remote_fetch_part_method($endpoint, $site_id, $part, $method, array $params = array()) {
  // Fetch.
  $methods = acton_profile_remote_get_fetch_methods();
  if (isset($methods[$method]) && function_exists($callback = $methods[$method]['fetch callback'])) {
    if ($result = $callback($endpoint, $site_id, $part, $params)) {
      return $result;
    }
  }
}

/**
 * Direct fetch callback.
 *
 * @param string $endpoint
 *   Style server endpoint (not including the protocol).
 * @param $site_id
 *   ANU site ID.
 * @param string $part
 *   Name of the part to fetch.
 * @param array $params
 *   Extra HTTP parameters.
 */
function acton_profile_remote_fetch_direct_get($endpoint, $site_id, $part, array $params = array()) {
  // Get base URL.
  $params['id'] = $site_id;
  $params['part'] = $part;
  $url = acton_profile_remote_fetch_direct_get_url($endpoint, $params);

  // Fetch.
  $result = drupal_http_request($url);
  return $result->data;
}

/**
 * Construct base URL for direct fetch.
 */
function acton_profile_remote_fetch_direct_get_url($endpoint, array $params = array()) {
  // Normalise parameters.
  $params += array(
    'ver' => acton_profile_style_version(),
    'nojq' => 1,
  );
  $params_clean = array();
  foreach ($params as $key => $value) {
    if ($value) {
      $params_clean[$key] = $value;
    }
  }
  ksort($params_clean);

  // Construct URL.
  $protocol = !empty($params_clean['ssl']) ? 'https' : 'http';
  $base_url = $protocol . '://' . rtrim($endpoint, '?') . '?' . http_build_query($params_clean, '', '&');
  return $base_url;
}

/**
 * Direct fetch callback.
 *
 * @param string $endpoint
 *   Style server endpoint (not including the protocol).
 * @param $site_id
 *   ANU site ID.
 * @param string $part
 *   Name of the part to fetch.
 * @param array $params
 *   Extra HTTP parameters.
 */
function acton_profile_remote_fetch_cache_get($endpoint, $site_id, $part, array $params = array()) {
  $params_id = _acton_profile_params_hash($params);
  $cid = "$endpoint:$site_id:$part:$params_id";

  // Fetch.
  $parts = &drupal_static(__FUNCTION__);
  if (!isset($parts)) {
    $parts = array();
  }
  $static = &$parts[$cid];
  if (!isset($static)) {
    $cache = cache_get($cid, 'cache_acton_profile');
    if (is_object($cache) && $cache->data && REQUEST_TIME < $cache->expire) {
      // Use Drupal cache.
      $static = $cache->data;
    }
    else {
      // Fetch part.
      $fetch_part = acton_profile_remote_fetch_direct_get($endpoint, $site_id, $part, $params);
      if ($fetch_part) {
        // Only validate fetched part if not empty.
        $static = $fetch_part;
        cache_set($cid, $static, 'cache_acton_profile', REQUEST_TIME + variable_get('acton_profile_cache_lifetime', ACTON_PROFILE_DEFAULT_CACHE_LIFE));
      }
    }
  }

  return $static;
}

/**
 * Get an array of fetch method info declarations.
 */
function acton_profile_remote_get_fetch_methods() {
  $methods = &drupal_static(__FUNCTION__);
  if (!isset($methods)) {
    $cache = cache_get($cid = 'acton_profile:fetch_methods');
    if (isset($cache->data)) {
      $methods = $cache->data;
    }
    else {
      // Build fetch methods.
      $methods = array();
      foreach (module_implements('acton_profile_fetch_methods') as $module) {
        if (is_array($info = module_invoke($module, 'acton_profile_fetch_methods'))) {
          foreach ($info as $key => $method) {
            $methods[$key] = $method;
          }
        }
      }
      uasort($methods, 'drupal_sort_weight');
      cache_set($cid, $methods);
    }
  }

  return $methods;
}

/**
 * Get an MD5 hash for a params array.
 */
function _acton_profile_params_hash(array $params) {
  $params = array_filter($params);
  ksort($params);
  return md5(serialize($params));
}
