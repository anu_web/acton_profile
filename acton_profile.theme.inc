<?php
/**
 * @file
 * Theme hooks and callbacks to be included with module.
 */

/**
 * Insert variables into HTML.
 */
function acton_profile_preprocess_html(&$variables) {
  // Give the Acton theme the currently active profile.
  $profile = acton_profile_get_active_profile();
  $variables['acton_profile'] = $profile;
}

/**
 * Insert variables into page.
 */
function acton_profile_preprocess_page(&$variables) {
  // Give the Acton theme the currently active profile.
  $profile = acton_profile_get_active_profile();
  if (!$profile) {
    acton_profile_set_alert(t('Please configure a !default_profile.', array('!default_profile' => l(t('default profile'), 'admin/appearance/acton-profile'))));
  }
  $variables['acton_profile'] = $profile;
}

/**
 * Process variables for page.
 */
function acton_profile_process_page(&$variables) {
  $variables['alert_bar_message'] = acton_profile_get_alert();
}

/**
 * Insert variables into left region (menubar).
 */
function acton_profile_preprocess_region(&$variables) {
  if ($variables['region'] == 'sidebar_first') {
    // Give the Acton theme the currently active profile.
    $profile = acton_profile_get_active_profile();
    $variables['acton_profile'] = $profile;
  }
}

/**
 * Clean string variables.
 */
function _acton_clean_variables(&$variables, array $keys) {
  foreach ($keys as $key) {
    if (is_scalar($key) && $variables[$key]) {
      $variables[$key] = check_plain($variables[$key]);
    }
  }
}

/**
 * Process tab links.
 */
function acton_profile_preprocess_link(&$variables) {
  if (!empty($variables['options']['acton_banner_tab']) && !empty($variables['options']['attributes']['class'])) {
    $class = &$variables['options']['attributes']['class'];
    if (in_array('active', $class)) {
      $class[] = 'tabs-select';
    }
  }
}
