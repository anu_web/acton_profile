<?php
/**
 * @file
 * Acton profile data.
 */

/**
 * Implements hook_acton_profile_data().
 */
function acton_profile_acton_profile_data() {
  $data = array();

  $data['site_short_name'] = array(
    'label' => t('Site short name'),
    'description' => t('An optionally shorter version of the site title to appear in the browser title bar and the search box title.'),
    'required' => TRUE,
  );
  $data['site_id'] = array(
    'label' => t('Site ID'),
    'description' => t('This is the identifier provided to you when you apply for this site. For a list of possible site IDs, please visit the !styleguide website.', array('!styleguide' => l(t('Web Style Guide'), 'http://styles.anu.edu.au/guide/id.php', array('attributes' => array('target' => '_blank'))))),
    'required' => TRUE,
  );
  $data['site_url'] = array(
    'label' => t('Site URL'),
    'description' => t('This is the base URL that the style server links the site title to. Do not include the protcol (e.g. http://). Example site URL: www.anu.edu.au/site'),
    'required' => TRUE,
  );
  $data['resp_officer'] = array(
    'label' => t('Responsible Officer'),
    'description' => t('The position with final responsibility for the site. Should not be role name, not an individual.'),
    'required' => TRUE,
  );
  $data['resp_officer_address'] = array(
    'label' => t('Responsible Officer URL/Email'),
    'description' => t('An email address or contact page for the Responsible Officer. The Contact module provides a default contact form at "contact" which you may wish to use. Email addresses are checked for validity, but not obfuscated.'),
    'required' => TRUE,
  );
  $data['site_contact'] = array(
    'label' => t('Site Contact'),
    'description' => t('The principal contact for queries relating to the site (e.g. Webmaster).'),
    'required' => TRUE,
  );
  $data['site_contact_address'] = array(
    'label' => t('Site Contact URL/Email'),
    'description' => t('An email address or contact page for the Responsible Officer. The Contact module provides a default contact form at "contact" which you may wish to use. Email addresses are checked for validity, but not obfuscated.'),
    'required' => TRUE,
  );
  $data['date_modified'] = array(
    'label' => t('Date modified'),
    'type' => 'date',
    'description' => t('The date this site has last changed. This date should reflect the day when the site is officially live. This date will be used if a page does not override it.'),
    'required' => TRUE,
  );
  $data['date_review'] = array(
    'label' => t('Date to review'),
    'type' => 'date',
    'description' => t('The date this site is due for review. This date should be some time after the modified date.'),
    'required' => TRUE,
  );
  $data['meta_description'] = array(
    'label' => t('Default description'),
    'description' => t('Description of the page or site to be used where no page description is provided. A common use of this value is Google search results descripton.'),
  );
  $data['meta_subject'] = array(
    'label' => t('Default subject'),
    'description' => t('Default subject keywords where no specific keywords can be derived, separated by commas. Note that most search engines ignore this value.'),
  );
  $data['about_page'] = array(
    'label' => t('About page'),
    'description' => t('Path to the About page. The About page is typically used to indicate the website compliance with standards and any help or instructions specific to using the site. It is NOT the site\'s About us page.'),
  );

  return $data;
}

/**
 * Implements hook_acton_profile_fetch_methods().
 */
function acton_profile_acton_profile_fetch_methods() {
  $methods['direct_get'] = array(
    'label' => t('Direct (via HTTP GET)'),
    'fetch callback' => 'acton_profile_remote_fetch_direct_get',
    'weight' => -10,
  );
  $methods['cache_get'] = array(
    'label' => t('Cached GET'),
    'fetch callback' => 'acton_profile_remote_fetch_cache_get',
    'weight' => -5,
  );

  return $methods;
}
