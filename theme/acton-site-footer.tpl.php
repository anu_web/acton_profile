<?php
/**
 * @file
 * Acton site footer.
 *
 * Variables:
 * - $resp_officer: Title of the Responsible Officer.
 * - $resp_officer_address: Contact address of the Responsible Officer.
 * - $site_contact: Title of the Site Contact.
 * - $site_contact_address: Contact address of the Site Contact.
 * - $date_modified: Date the current page was last updated.
 * - $about_page: Link to the about page.
 */
?>
<div id="update-wrap">
	<div id="update-details">
		<p class="sml-hdr">
			<?php if ($about_page): ?>
        <span class="upd-about"><?php print l('<strong>' . t('About this site') . '</strong>', $about_page, array('absolute' => TRUE, 'html' => TRUE)); ?></span>
      <?php endif; ?>
      <?php print implode('<span class="hpad">/</span>', array_map('render', $parts)); ?>
		</p>
	</div>
</div>