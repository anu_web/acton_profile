<?php
/**
 * @file
 * ANU search box.
 *
 * Variables:
 * - $site_short_name: Site short name for displaying in browser title.
 * - $site_url: Site base URL.
 */
?>
<div class="search-box">
  <p>Search <?php print $site_short_name; ?></p>
  <form action="http://search.anu.edu.au/search/search.cgi" method="get">
    <div>
      <input name="scope" type="hidden" value="<?php print $scope; ?>" />
      <input name="collection" type="hidden" value="anu_search" />
      <label for="local-query"><span class="nodisplay">Search query</span></label><input class="search-query" name="query" id="local-query" size="15" type="text" value="" />
      <label for="search"><span class="nodisplay">Search</span></label><input class="search-button" id="search" title="Search" type="submit" value="GO" /><br/>
      <?php if ($advanced): ?>
        <a href="http://search.anu.edu.au/search/search.cgi?collection=anu_search&amp;form=advanced&amp;scope=<?php print $scope_query; ?>">Advanced search</a>
      <?php endif; ?>
    </div>
  </form>
</div>