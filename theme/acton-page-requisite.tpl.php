<?php
/**
 * @file
 * Render a page part.
 *
 * Variables:
 * - $part: Name of the part to render.
 * - $element: The prepared 'acton_profile_remote_part' element array.
 */

if ($element):
  print render($element);
endif;
