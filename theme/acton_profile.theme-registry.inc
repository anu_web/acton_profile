<?php
/**
 * @file
 * Acton Profile theme declaration and processing functions.
 */

/**
 * Declare theme hooks.
 */
function _acton_profile_theme($existing, $type, $theme, $path) {
  $theme_path = drupal_get_path('module', 'acton_profile') . '/theme';

  // Function theme hooks
  $hooks['acton_remote_part'] = array(
    'render element' => 'acton_remote_part',
    'path' => $theme_path,
    'file' => 'acton_profile.theme-registry.inc',
  );
  $hooks['acton_anu_head'] = array(
    'variables' => array('profile' => NULL),
    'path' => $theme_path,
    'file' => 'acton_profile.theme-registry.inc',
  );
  $hooks['acton_anu_body_header'] = array(
    'variables' => array('profile' => NULL),
    'path' => $theme_path,
    'file' => 'acton_profile.theme-registry.inc',
  );
  $hooks['acton_anu_body_footer'] = array(
    'variables' => array('profile' => NULL),
    'path' => $theme_path,
    'file' => 'acton_profile.theme-registry.inc',
  );

  // Prepare variables
  $variable_sets = array();
  $variable_sets['acton_page_requisite'] = array(
    'part' => '',
    'profile' => NULL,
  );
  $variable_sets['acton_head'] = array(
    'identifier' => '',
    'title' => '',
    'description' => '',
    'subject' => '',
    'keywords' => '',
    'date_modified' => '',
    'date_review' => '',
    'creator' => '',
    'creator_email' => '',
  );
  $variable_sets['acton_alert_bar'] = array(
    'message' => '',
  );
  $variable_sets['acton_banner_tabs'] = array(
    'links' => array(),
  );
  $variable_sets['acton_search_box'] = array(
    'site_short_name' => '',
    'search_base' => array(),
    'advanced' => TRUE,
  );
  $variable_sets['acton_search_box_mini'] = array(
    'search_base' => array(),
  );
  $variable_sets['acton_site_footer'] = array(
    'resp_officer' => '',
    'resp_officer_address' => '',
    'site_contact' => '',
    'site_contact_address' => '',
    'date_modified' => '',
    'about_page' => '',
  );

  // All other theme hooks
  foreach ($variable_sets as $hook => $variables) {
    $hooks[$hook] = array(
      'variables' => $variables,
      'template' => strtr($hook, '_', '-'),
      'path' => $theme_path,
      'file' => 'acton_profile.theme-registry.inc',
    );
  }

  return $hooks;
}

/**
 * Insert variables for remote part.
 */
function template_preprocess_acton_remote_part(&$variables) {
  $element = &$variables['acton_remote_part'];
  $variables['element'] = &$element;
  $variables['part'] = $element['#part'];
}

/**
 * Render the part content.
 *
 * @param $variables
 *   Variables to render:
 *   - 'element': The source render array.
 *   - 'part': The page part to render.
 *
 * @return
 *   HTML content.
 */
function theme_acton_remote_part(&$variables) {
  $element = &$variables['element'];
  $part = $element['#part'];
  $profile = $element['#profile'];
  $params = $element['#params'];

  // Sanity checks
  if ($part && is_object($profile) && $profile instanceof ActonProfile && is_array($params)) {
    $variables['content'] = acton_profile_remote_get_part($part, $profile, $params);
  }

  return $variables['content'];
}

/**
 * Insert variables into 'acton_page_requisite'.
 */
function template_preprocess_acton_page_requisite(&$variables) {
  if ($variables['part']) {
    // Add template suggestion
    $variables['theme_hook_suggestions'][] = 'acton_page_requisite__' . $variables['part'];

    // Build render element
    global $is_https;
    $variables['element'] = array(
      '#type' => 'acton_remote_part',
      '#part' => $variables['part'],
      '#profile' => $variables['profile'],
      '#params' => array('ssl' => $is_https ? 1 : 0),
    );
  }
}

/**
 * Finalise variables in 'acton_head'.
 */
function template_process_acton_head(&$variables) {
  // Validate creator email
  if (!valid_email_address($variables['creator_email'])) {
    $variables['creator_email'] = '';
  }

  // check_plain() the rest
  $clean = array('identifier', 'title', 'description', 'subject', 'keywords', 'date_modified', 'date_review', 'creator', 'creator_email');
  _acton_clean_variables($variables, $clean);
}

/**
 * Adds local jQuery support for ANU scripts.
 */
function template_preprocess_acton_anu_head(&$variables) {
  $profile = $variables['profile'];
  $style_version = $profile->getStyleVersion();
  $js_base = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $profile->getServerEndpointBase() . '/' . $style_version;

  // Add centrally hosted jQuery plugins.
  drupal_add_js($js_base . '/scripts/jquery.hoverIntent.js', array(
    'type' => 'external',
    'group' => JS_LIBRARY,
    'weight' => 5,
  ));

  // Alias jQuery library.
  drupal_add_js('var $anujq = jQuery.noConflict();', array(
    'type' => 'inline',
    'group' => JS_LIBRARY,
    'weight' => 5,
  ));

  // Add style scripts.
  drupal_add_js($js_base . '/scripts/anu-common.js', array(
    'type' => 'external',
    'weight' => 5,
  ));

  if ($style_version > 3) {
    drupal_add_js($js_base . '/scripts/modernizr-2.0.6.min.js', array(
      'type' => 'external',
      'weight' => 5,
    ));
    drupal_add_js($js_base . '/scripts/anu-menu.js', array(
      'type' => 'external',
      'weight' => 5,
    ));
  }
}

/**
 * Render 'theme_acton_anu_head'.
 */
function theme_acton_anu_head(&$variables) {
  return theme('acton_page_requisite', array(
    'part' => 'meta',
    'profile' => $variables['profile'],
  ));
}

/**
 * Render 'acton_anu_body_header'.
 */
function theme_acton_anu_body_header(&$variables) {
  return theme('acton_page_requisite', array(
    'part' => 'banner',
    'profile' => $variables['profile'],
  ));
}

/**
 * Render 'acton_anu_body_footer'.
 */
function theme_acton_anu_body_footer(&$variables) {
  return theme('acton_page_requisite', array(
    'part' => 'footer',
    'profile' => $variables['profile'],
  ));
}

/**
 * Insert variables in 'acton_search_box'.
 */
function template_preprocess_acton_search_box(&$variables) {
  // Prepare search scope
  $bases = array();
  foreach ($variables['search_base'] as $base) {
    if ($base) {
      // Encode commas
      $bases[] = str_replace(',', '%2C', $base);
    }
  }
  $scope = implode(',', $bases);

  $variables['scope_raw'] = $scope;
  $variables['scope'] = check_plain($scope);
  $variables['scope_query'] = check_plain(urlencode($scope));
}

/**
 * Process variables in 'acton_search_box'.
 */
function template_process_acton_search_box(&$variables) {
  _acton_clean_variables($variables, array('site_short_name'));
}

/**
 * Process variables in 'acton_banner_tabs'.
 */
function template_process_acton_banner_tabs(&$variables) {
  // Process menu links into tab links.
  $variables['tab_links'] = array();
  foreach ($variables['links'] as $name => $link) {
    $link['acton_banner_tab'] = TRUE;
    $variables['tab_links'][$name] = l($link['title'], $link['href'], $link);
  }
}

/**
 * Insert variables in 'acton_search_box_mini'.
 */
function template_preprocess_acton_search_box_mini(&$variables) {
  // Prepares search scope as regular search box.
  template_preprocess_acton_search_box($variables);
}

/**
 * Insert variables in 'acton_site_footer'.
 */
function template_preprocess_acton_site_footer(&$variables) {
  if (valid_email_address($variables['resp_officer_address'])) {
    $variables['resp_officer_address'] = 'mailto:' . $variables['resp_officer_address'];
  }
  if (valid_email_address($variables['site_contact_address'])) {
    $variables['site_contact_address'] = 'mailto:' . $variables['site_contact_address'];
  }
}

/**
 * Process variables in 'acton_site_footer'.
 */
function template_process_acton_site_footer(&$variables) {
  $clean = array('resp_officer', 'resp_officer_address', 'site_contact', 'site_contact_address', 'date_modified');
  _acton_clean_variables($variables, $clean);

  // Populate site footer parts.
  $variables['parts'] = array();
  if ($variables['date_modified']) {
    $variables['parts']['date_modified'] = array(
      '#prefix' => '<span class="upd-date">',
      '#markup' => t('Updated:') . '&nbsp;&nbsp;<strong>' . $variables['date_modified'] . '</strong>',
      '#suffix' => '</span>',
    );
  }
  if ($variables['resp_officer'] && $variables['resp_officer_address']) {
    $variables['parts']['resp_officer'] = array(
      '#prefix' => '<span class="upd-officer">',
      '#markup' => t('Responsible Officer:') . '&nbsp;&nbsp;' . l('<strong>' . $variables['resp_officer'] . '</strong>', $variables['resp_officer_address'], array('html' => TRUE)),
      '#suffix' => '</span>',
    );
  }
  if ($variables['site_contact'] && $variables['site_contact_address']) {
    $variables['parts']['site_contact'] = array(
      '#prefix' => '<span class="upd-contact">',
      '#markup' => t('Page Contact:') . '&nbsp;&nbsp;' . l('<strong>' . $variables['site_contact'] . '</strong>', $variables['site_contact_address'], array('html' => TRUE)),
      '#suffix' => '</span>',
    );
  }
}
