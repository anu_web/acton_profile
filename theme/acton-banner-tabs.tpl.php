<?php
/**
 * @file
 * Horizontal tabs navigation template.
 */
?>
<div id="tabs-wrap">
  <div id="tabs-nav">
    <div class="left padleft padright">
      <ul>
        <?php foreach ($tab_links as $link): ?>
          <li><?php print $link; ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>
