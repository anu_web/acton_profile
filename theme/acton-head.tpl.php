<?php
/**
 * @file
 * ANU styles HTML head.
 *
 * Variables:
 * - $identifier: Page identifier (e.g. canonical URL).
 * - $title: Page title (for use in window title).
 * - $description: Page general description.
 * - $subject: Page content subject line.
 * - $keywords: Related keywords.
 * - $date_modified: Date page was last modified.
 * - $date_review: Date page is due for review.
 * - $creator: Creator's name.
 * - $creator_email: Creator's contact email address.
 *
 * @see template_preprocess_acton_head()
 * @see template_process_acton_head()
 */
?>
<?php if ($title): ?>
  <meta name="DC.title" content="<?php print $title; ?>" />
<?php endif; ?>
<?php if ($identifier): ?>
  <meta name="DC.identifier" scheme="URI" content="<?php print $identifier; ?>" />
<?php endif; ?>
<?php if ($description): ?>
  <meta name="DC.description" content="<?php print $description; ?>" />
<?php endif; ?>
<?php if ($subject): ?>
  <meta name="DC.subject" content="<?php print $subject; ?>" />
<?php endif; ?>
<?php if ($date_modified): ?>
  <meta name="DC.date.modified" scheme="ISO8601" content="<?php print $date_modified; ?>" />
<?php endif; ?>
<?php if ($date_review): ?>
  <meta name="DC.date.review" scheme="ISO8601" content="<?php print $date_review; ?>" />
<?php endif; ?>
<?php if ($creator): ?>
  <meta name="DC.creator" content="<?php print $creator; ?>" />
<?php endif; ?>
<?php if ($creator_email): ?>
  <meta name="DC.creator.email" content="<?php print $creator_email; ?>" />
<?php endif; ?>
<?php if ($description): ?>
  <meta name="description" content="<?php print $description; ?>" />
<?php endif; ?>
<?php if ($subject): ?>
  <meta name="subject" content="<?php print $subject; ?>" />
<?php endif; ?>
<?php if ($keywords): ?>
  <meta name="keywords" content="<?php print $keywords; ?>" />
<?php endif; ?>