<?php
/**
 * @file
 * ANU mini search box for responsive design.
 *
 * Variables:
 * - $site_url: Site base URL.
 *
 * @var $site_url string
 */
?>
<div class="search-form">
  <form action="http://search.anu.edu.au/search/search.cgi" method="get">
    <input name="scope" type="hidden" value="<?php print $scope; ?>" />
    <input name="collection" type="hidden" value="anu_search" />
    <label for="local-query-mini">search</label> <input class="search-query" name="query" id="local-query-mini" size="15" type="text" value="" />
    <label for="search-mini"><span class="nodisplay">search</span></label><input class="search-button" id="search-mini" title="Search" type="submit" value="GO" />
  </form>
</div>
