<?php
/**
 * @file
 * Alert bar.
 *
 * Variables:
 * - $message: Alert bar message text.
 */

if ($message):
?>
<div id="devlmsg">
  <?php print $message; ?>
</div>
<?php
endif;