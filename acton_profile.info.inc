<?php
/**
 * @file
 * Entity API property declarations.
 */

/**
 * Implements hook_entity_property_info().
 */
function acton_profile_entity_property_info() {
  // Use inline link rendering to prevent infinite loop.
  variable_set('theme_link', FALSE);

  $info = array();
  $properties = &$info['acton_profile']['properties'];

  // Define basic metadata.
  $properties['pid'] = array(
    'label' => t('Profile ID'),
    'description' => t('The serial identifier of the profile.'),
    'type' => 'integer',
    'schema field' => 'pid',
  );
  $properties['machine_name'] = array(
    'label' => t('Machine name'),
    'description' => t('The machine-readable profile name.'),
    'type' => 'token',
    'required' => TRUE,
    'schema field' => 'machine_name',
  );
  $properties['name'] = array(
    'label' => t('Name'),
    'description' => t('The human-readable name of the profile.'),
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'name',
  );

  // Define profile data.
  $properties['data'] = array(
    'label' => t('Data'),
    'type' => 'struct',
    'getter callback' => 'acton_profile_property_cannot_get',
  );

  $data = acton_profile_get_profile_data();
  foreach ($data as $key => $property) {
    $properties['data']['property info'][$key] = $property + array(
      'getter callback' => 'acton_profile_property_data_get',
      'setter callback' => 'acton_profile_property_data_set',
    );
  }

  // Restore link rendering pipeline.
  variable_del('theme_link');
  return $info;
}
