<?php
/**
 * @file
 * Rules integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function acton_profile_rules_action_info() {
  $actions = array();

  $actions['get_active_profile'] = array(
    'label' => t('Get active profile'),
    'provides' => array(
      'profile_active' => array(
        'type' => 'acton_profile',
        'label' => t('Active profile'),
      ),
    ),
    'help' => t('Get the currently active profile.'),
    'group' => t('Acton Profile'),
    'base' => 'acton_profile_get_active_profile',
    'module' => 'acton_profile',
  );
  $actions['fetch'] = array(
    'label' => t('Fetch profile by name'),
    'parameter' => array(
      'machine_name' => array(
        'type' => 'text',
        'label' => t('Profile name'),
        'options list' => 'acton_profile_rules_profile_options',
        'restriction' => 'input',
      ),
    ),
    'provides' => array(
      'profile_fetched' => array(
        'type' => 'acton_profile',
        'label' => t('Fetched profile'),
      ),
    ),
    'help' => t('Set a profile to active. Combine this action with custom conditions to use different profiles in different sections.'),
    'group' => t('Acton Profile'),
    'base' => 'acton_profile_load',
    'module' => 'acton_profile',
  );
  $actions['set_active_profile'] = array(
    'label' => t('Set profile as active'),
    'parameter' => array(
      'acton_profile' => array(
        'type' => 'acton_profile',
        'label' => t('The profile to set active.'),
        'restriction' => 'selector',
      ),
    ),
    'help' => t('Set a profile to active. Combine this action with custom conditions to use different profiles in different sections.'),
    'group' => t('Acton Profile'),
    'base' => 'acton_profile_set_active_profile',
    'module' => 'acton_profile',
  );
  $actions['set_alert_message'] = array(
    'label' => t('Set alert bar message'),
    'parameter' => array(
      'message' => array(
        'type' => 'text_formatted',
        'label' => t('The message to insert in the alert bar.'),
      ),
    ),
    'help' => t('Set an alert bar message to in the alert bar.'),
    'group' => t('Acton Profile'),
    'base' => 'acton_profile_rules_set_alert',
    'module' => 'acton_profile',
  );

  return $actions;
}

/**
 * Get a list of profiles to select from.
 *
 * @param $element
 *   The element to return options for.
 * @param $param
 *   The name of the parameter to return options for.
 */
function acton_profile_rules_profile_options($element, $name = NULL) {
  $options = array();
  foreach (acton_profile_load_multiple() as $key => $profile) {
    $options[$key] = $profile->name;
  }
  return $options;
}

/**
 * Set alert bar message.
 */
function acton_profile_rules_set_alert($message) {
  $message_contents = check_markup($message['value'], $message['format']);
  acton_profile_set_alert($message_contents);
}
