<?php
/**
 * @file
 * Extra tokens integration (in addition to Entity API).
 */

/**
 * Implements hook_token_info().
 */
function acton_profile_token_info() {
  // TODO Define active profile tokens.

  $info['types']['acton-profile'] = array(
    'name' => t('Acton profile'),
    'description' => t('Tokens related to Acton profiles.'),
    'needs-data' => 'acton_profile',
  );

  $info['tokens']['acton-profile']['machine-name'] = array(
    'name' => t('Machine name'),
    'description' => t('The unique machine-readable name of the Acton profile.'),
  );

  // The rest of the tokens (i.e. those under 'data') should be available once
  // http://drupal.org/node/1183676 is resolved.

  return $info;
}

/**
 * Implements hook_tokens().
 */
function acton_profile_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Define profile tokens.
  if ($type == 'acton-profile' && !empty($data['acton-profile'])) {
    $acton_profile = $data['acton-profile'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'machine-name':
          // This is a machine name so does not ever need to be sanitized.
          $replacements[$original] = $acton_profile->machine_name;
          break;
      }
    }
  }

  return $replacements;
}
