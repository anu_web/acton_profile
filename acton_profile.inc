<?php
/**
 * @file
 * Entity implementations.
 */

/**
 * Acton profile entity class.
 */
class ActonProfile extends Entity {
  /**
   * Serial identifier.
   * @var int
   */
  public $pid;

  /**
   * Machine-readable name.
   * @var string
   */
  public $machine_name;

  /**
   * Profile name.
   * @var string
   */
  public $name;

  /**
   * Profile data array.
   * @var array
   */
  public $data;

  /**
   * Profile settings array.
   * @var array
   */
  public $settings;

  /**
   * Constructor.
   */
  public function __construct(array $values = array()) {
    parent:: __construct($values, 'acton_profile');
    if (!empty($values['is_new'])) {
      $this->data = array();
      $this->settings = array();
    }
  }

  /**
   * Determine the style server endpoint base to use.
   */
  public function getServerEndpointBase(array $submitted = array()) {
    $base = isset($submitted['server_endpoint_base']) ? $submitted['server_endpoint_base'] : $this->settings['server_endpoint_base'];
    $override = isset($submitted['server_override']) ? $submitted['server_override'] : $this->settings['server_override'];
    if (!$override || !$base) {
      $base = variable_get('acton_profile_default_endpoint_base', ACTON_PROFILE_DEFAULT_ENDPOINT_BASE);
    }
    return $base;
  }

  /**
   * Determine the style server endpoint suffix to use.
   */
  public function getServerEndpointSuffix(array $submitted = array()) {
    $suffix = isset($submitted['server_endpoint_suffix']) ? $submitted['server_endpoint_suffix'] : $this->settings['server_endpoint_suffix'];
    $override = isset($submitted['server_override']) ? $submitted['server_override'] : $this->settings['server_override'];
    if (!$override || !$suffix) {
      $suffix = variable_get('acton_profile_default_endpoint_suffix', ACTON_PROFILE_DEFAULT_ENDPOINT_SUFFIX);
    }
    return $suffix;
  }

  /**
   * Determine the style server endpoint to use.
   */
  public function getServerEndpoint(array $submitted = array()) {
    // Extract fetch parameters from profile.
    $endpoint_base = $this->getServerEndpointBase($submitted);
    $endpoint_suffix = $this->getServerEndpointSuffix($submitted);
    if (!acton_profile_valid_endpoint($endpoint = $endpoint_base . $endpoint_suffix)) {
      // Fall back to hard-coded default.
      $endpoint = ACTON_PROFILE_DEFAULT_ENDPOINT_BASE . ACTON_PROFILE_DEFAULT_ENDPOINT_SUFFIX;
    }

    return $endpoint;
  }

  /**
   * Returns the style version for this profile.
   */
  public function getStyleVersion() {
    return !empty($this->settings['style_version']) ? $this->settings['style_version'] : acton_profile_style_version();
  }
}

/**
 * Controller class for Acton profiles.
 */
class ActonProfileController extends EntityAPIControllerExportable {
  /**
   * Augment default query. Do not support revisions.
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    $query = parent::buildQuery($ids, $conditions);
    $query->addTag('translatable');
    $query->orderBy('base.name');
    return $query;
  }
}
